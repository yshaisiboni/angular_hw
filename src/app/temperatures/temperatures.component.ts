import { WeatherService } from './../weather.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Weather } from '../interfaces/weather';
import { Observable } from 'rxjs';

@Component({
  selector: 'temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {
  city:string;
  temperature:number;
  image:string;
  weatherData$:Observable<Weather>;
  country;
  description;
  lat;
  lon;
  hasError:Boolean = false;
  errorMessage:string;
  constructor(private route:ActivatedRoute, private WeatherService:WeatherService) { }

  ngOnInit(): void {
    this.city = this.route.snapshot.params.city;
    this.weatherData$ = this.WeatherService.searchWeatherData(this.city);
    this.weatherData$.subscribe(
      data => { 
        this.temperature = data.temperature;
        this.image = data.image;
        this.country = data.country;
        this.description = data.description;
        this.lat = data.lat;
        this.lon = data.lon;
      },
      error => {
        console.log(error.message);
        this.hasError = true;
        this.errorMessage = error.message;
      }
      )
  }

}
