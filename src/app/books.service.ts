import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  bookCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');

  public getBooks(userId, startAfter){
    this.bookCollection = this.db.collection(`users/${userId}/books` ,
    ref => ref.orderBy('title', 'asc').limit(4).startAfter(startAfter));
    return this.bookCollection.snapshotChanges()
  }

  public getBooks2(userId, endBefore){
    this.bookCollection = this.db.collection(`users/${userId}/books` ,
    ref => ref.orderBy('title', 'asc').limitToLast(4).endBefore(endBefore));
    return this.bookCollection.snapshotChanges()
  }

  /*
  public getBooks(userId){
    this.bookCollection = this.db.collection(`users/${userId}/books`);
    return this.bookCollection.snapshotChanges().pipe(map(
      collection =>collection.map(
        document => {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data;
        }
      )
    )) //the first map -> Hamara function
  }
  */

  public deleteBook(Userid:string, id:string){
    this.db.doc(`users/${Userid}/books/${id}`).delete();
  }

  public addBook(userId:string, title:string, author:string){
    const book = {title:title, author:author};
    this.userCollection.doc(userId).collection(`books`).add(book);

  }

  public updateBook(userId:string, id: string, title:string, author:string){
    this.db.doc(`users/${userId}/books/${id}`).update(
      {
      title:title,
      author:author
      }
    )
  }
  
  /* 
  //1
  books = [{title:'Alice in Wonderland', author:'Lewis Carrol', summary:'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s'},
           {title:'War and Peace', author:'Leo Tolstoy', summary:'when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries'},
           {title:'The Magic Mountain', author:'Thomas Mann', summary:'but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages'}];
  
  public addBooks(){
    setInterval(()=>this.books.push({title:'A new one', author:'New Author', summary:'A new summary'}),2000);
  }

  public getBooks(){
    const booksObservable = new Observable(obs => {
      setInterval(()=>obs.next(this.books),500)
    });
    return booksObservable;
  }

  
  public getBooks(){
    return this.books;
  }
  */

constructor(private db:AngularFirestore) { }


}