import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  
  private path:string = "https://firebasestorage.googleapis.com/v0/b/hello-jce-ishay.appspot.com/o/";
  public images:string[] = [];

  constructor() { 
    this.images[0] = this.path + 'biz.JPG' + '?alt=media&token=cf4ede3b-7d33-4557-99fe-d7b514cc4c47';
    this.images[1] = this.path + 'entermnt.JPG' + '?alt=media&token=d294d942-1d3d-49da-ac6a-4b5b951e1eca';
    this.images[2] = this.path +'politics-icon.png' + '?alt=media&token=8ffc1031-8a64-48dc-a969-841a9d1635c7';
    this.images[3] = this.path + 'sport.JPG' + '?alt=media&token=b68b95fd-7c6d-4310-970d-73b027bb51d2';
    this.images[4] = this.path + 'tech.JPG' + '?alt=media&token=2f3a54ca-762b-4f5f-a9a1-7e9e1189c2c9';
  }
}
