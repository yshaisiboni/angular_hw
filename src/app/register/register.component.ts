import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  hide = true;
  email:string;
  password:string;
  errorMessage:string;
  isError:boolean = false;


  onSubmit(){
    this.auth.register(this.email,this.password).then(res => {
      console.log(res);
      this.router.navigate(['/books'])   
      })
      .catch(error => {
        console.log(error);
        this.isError = true;
        this.errorMessage = error.message;
      })
    
    
  }

  constructor(private auth:AuthService, private router:Router) { }

  ngOnInit(): void {
    }

  }
