import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Posts } from './interfaces/posts';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  private URL = "https://jsonplaceholder.typicode.com/posts/";

  constructor(private http:HttpClient) { }

  getPosts():Observable<Posts>{
    return this.http.get<Posts>(`${this.URL}`).pipe(
      catchError(this.handleError)
    )
  }

  private handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError(res.error || 'Server Error');
  }

}

